# logseq-plugin-starter-template
A quick and easy to use template to get you going from the get go

Referenced here:
https://discuss.logseq.com/t/hrishi-earth-system-scientist-struggling-artist/3090/8
Written out:
From a post to the Logseq forum: 
This is the code-structure that GPT engineer gave me - still using github in this case :innocent: -  and here is the follow-up chat with Chat GPT to bug fix it. 
https://chat.openai.com/share/22d490d8-b613-4b5b-a2b9-914d980bf61c
As you can see from there, the slash-command does not work, but the original one from Aryan is still in there. 
https://www.youtube.com/watch?v=57h7te3NvJg

(And here is another chat history where Chat GPT took a different approach.
https://chat.openai.com/share/6336d415-0eed-4d5f-8ddf-c045ffc6573b
 I stopped after several attempts of debugging the primary file {index.js}).


Status (for myself and maybe inspiring for others who are in a similar situation of desiring a Logseq-plugin:
For now, I will use the Obsidian-plugin for new long form text (so I can link to pages) and just continue with GingkoWriter for my existing notes - since the interface is better and I haven't yet found a way to import notes in a way that I like
(In case anyone reading this is wondering how to "get a link" to a gingko file:
 I timestamp new entries it like this YYYYMoMoDDHHMiMi to have a manual link to search and thus easy retrievability.)

