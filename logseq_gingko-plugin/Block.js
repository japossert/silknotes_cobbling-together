export class Block {
  constructor(id, content, children, parent) {
    this.id = id;
    this.content = content;
    this.children = children;
    this.parent = parent;
  }
}