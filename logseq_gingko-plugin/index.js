import { GingkoImporter } from './GingkoImporter.js';
import { LogseqGingkoView } from './LogseqGingkoView.js';

// Initialize the plugin
logseq.ready(async () => {
  const gingkoImporter = new GingkoImporter();
  const logseqGingkoView = new LogseqGingkoView();

  // Register the import command
  logseq.App.registerUIItem('toolbar', {
    key: 'import-gingko',
    title: 'Import Gingko',
    icon: 'upload',
    onClick: () => {
      gingkoImporter.importToLogseq();
    }
  });

  // Register the Gingko view command
  logseq.App.registerUIItem('toolbar', {
    key: 'view-gingko',
    title: 'View as Gingko',
    icon: 'eye',
    onClick: () => {
      logseqGingkoView.renderTree();
    }
  });
});

// Starter Template's main function (for reference or further customization)
import '@logseq/libs';

//Inputs 5 numbered blocks when called
async function insertSomeBlocks (e) {
  console.log('Open the calendar!')
  let numberArray = [1, 2, 3, 4, 5]
  for (const number in numberArray){
  logseq.App.showMsg("Function has been run")
  logseq.Editor.insertBlock(e.uuid, `This is block ${numberArray[number]}`, {sibling: true})}

  }
  

const main = async () => {
  console.log('plugin loaded');
  logseq.Editor.registerSlashCommand('insertBlocks', async (e) => {
    insertSomeBlocks(e)
  }
    
  )}

logseq.ready(main).catch(console.error);

// Register a new slash command for viewing leaves
logseq.App.registerUIItem("slash-commands", {
  key: "view-leaves",
  title: "View Leaves",
  type: "action",
  action: async () => {
    // Log to console
    console.log("Slash command '/view-leaves' triggered.");

    // Get the current block
    const currentBlock = await logseq.Editor.getCurrentBlock();

    // Insert a new block above the current block with the text "Successfully triggered view leaves"
    if (currentBlock && currentBlock.uuid) {
      await logseq.Editor.insertBlock("Successfully triggered view leaves", { before: currentBlock.uuid });
    }
  }
});
